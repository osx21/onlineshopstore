# Generated by Django 3.2.5 on 2021-09-02 07:44

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shopelements', '0024_delete_oder'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ['-cost']},
        ),
    ]
