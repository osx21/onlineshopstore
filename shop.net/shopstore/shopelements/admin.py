from django.contrib import admin
from django.contrib.auth.models import User
from .models import Product, Comment, Cart, CartProduct, Order

# class BlogAdmin(admin.ModelAdmin):
#    list_display = ['title', 'body']
#    prepopulated_fields = {'slug': ('text')

class CommentAdmin(admin.ModelAdmin):
	list_display = ['fullname', 'date']
class ProductAdmin(admin.ModelAdmin):
	list_display = ['name', 'firma', 'cost', 'colour', 'date', 'made_year']


admin.site.register(Product, ProductAdmin)
admin.site.register(Order)
admin.site.register(Comment, CommentAdmin)
admin.site.register(Cart)
admin.site.register(CartProduct)