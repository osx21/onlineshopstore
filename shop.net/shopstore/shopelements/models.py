from django.db import models
import datetime
from django.urls import reverse

class Product(models.Model):
	name = models.CharField(max_length=50)
	text = models.TextField()
	firma = models.CharField(max_length=70)
	cost = models.PositiveIntegerField()
	COLOR_CHOICES = (
		("red", 'red'),
		("green", 'green'),
		("blue", 'blue'),
		("pink", 'pink'),
		("black", 'black'),
		("white", 'white'),
		("brown", 'brown'),
		("yellow", 'yellow')
	)
	colour = models.CharField(choices=COLOR_CHOICES, max_length=10, default='qizil')
	date = models.DateTimeField(auto_now=True)
	image = models.ImageField(upload_to='static/uploads/', null=True)
	YEAR_CHOICES = [(r,r) for r in range(1984, datetime.date.today().year+1)]
	made_year = models.IntegerField('year', choices=YEAR_CHOICES, default=datetime.datetime.now().year)

	def __str__(self):
		return self.name + '(' + self.firma + ')'
	class Meta:
		ordering = ['-cost']


class Comment(models.Model):
	fullname = models.CharField(max_length=50, null=True)
	text = models.TextField(null=True)
	product = models.ForeignKey(Product, on_delete=models.CASCADE)
	date = models.DateTimeField(auto_now=True)

	def get_absolute_url(self):
		return f'/product/{self.product.pk}/'
	class Meta:
		ordering = ['-date']

class Cart(models.Model):
    total = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "Cart: " + str(self.id)


class CartProduct(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    rate = models.PositiveIntegerField()
    quantity = models.PositiveIntegerField()
    subtotal = models.PositiveIntegerField()

    def __str__(self):
        return "Cart: " + str(self.cart.id) + " CartProduct: " + str(self.id)


ORDER_STATUS = (
    ("Order Received", "Order Received"),
    ("Order Processing", "Order Processing"),
    ("On the way", "On the way"),
    ("Order Completed", "Order Completed"),
    ("Order Canceled", "Order Canceled"),
)

METHOD = (
    ("Cash On Delivery", "Cash On Delivery"),
    ("Khalti", "Khalti"),
    ("Esewa", "Esewa"),
)


class Order(models.Model):
    cart = models.OneToOneField(Cart, on_delete=models.CASCADE, unique=True)
    ordered_by = models.CharField(max_length=200)
    shipping_address = models.CharField(max_length=200)
    mobile = models.CharField(max_length=10)
    email = models.EmailField(null=True, blank=True)
    subtotal = models.PositiveIntegerField()
    discount = models.PositiveIntegerField()
    total = models.PositiveIntegerField()
    order_status = models.CharField(max_length=50, choices=ORDER_STATUS)
    created_at = models.DateTimeField(auto_now_add=True)
    payment_method = models.CharField(
        max_length=20, choices=METHOD, default="Cash On Delivery")
    payment_completed = models.BooleanField(
        default=False, null=True, blank=True)

    def __str__(self):
        return "Order: " + str(self.id)




