"""shopstore URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from shopelements import urls
from shopelements import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.HomePageView, name='home'),
    path('about/', views.AboutPageView, name='about'),
    path('brand/', views.BrandPageView, name='brand'),
    path('specials/', views.SpecialsPageView, name='specials'),
    path('product/<int:pk>/', views.DetailPageView.as_view(), name='detail'),
    path('product/<int:pk>/comment/', views.comment,name='comment_url'),
    path('product/comment/update/<int:pk>/', views.CommentUpdatePageView.as_view(), name='comment_update'),
    path('search_venues/', views.search_venues, name='search-venues'),
    path('', include('shopelements.urls', namespace='ecomapp'))
]
